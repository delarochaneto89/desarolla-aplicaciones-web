**Desarrollo de aplicaciones web con conexion a bases de datos**
**De La Rocha Alvarez Ernesto Alonso**
**5AVP**

-Practica #1 - 02/09/2022 - Practica de ejemplo
Commit: 4d6500b7691899640ba18782e50d87f8c4124806
Archivo: https://gitlab.com/delarochaneto89/desarolla-aplicaciones-web/-/blob/main/parcial1/parcial1/practica_ejemplo.html

-Practica #2 - 09/09/2022 - Practica JavaScript
Commit: a1af669054eab4f0963511902a5acbbb7e5cea07
Archivo: https://gitlab.com/delarochaneto89/desarolla-aplicaciones-web/-/blob/main/parcial1/parcial1/PracticaJavaScript.html

-Practica #3 - 15/09/2022 - Practica WebDatos
Commit: 00f84b1a2ab1747d0902bf65dabe9307115acdda
Archivo: https://gitlab.com/delarochaneto89/desarolla-aplicaciones-web/-/blob/main/parcial1/parcial1/PracticaWebDatos.rar

-Practica #4 - 19/09/2022 - Practica con bases de datos - Visita de consulta de datos Commit:198a00e79ed85246242a3f25895150ce8bb6a217
Archivo: https://gitlab.com/delarochaneto89/desarolla-aplicaciones-web/-/blob/main/parcial1/parcial1/PracticaWebDatos/consultarDatos.php

-Practica #5 - 22/09/2022 - Practica web con base de datos - Visita de consulta de datos Commit:a2cb4f8b238acaaa77e55c6df84b658a25d54877
Archivo: https://gitlab.com/delarochaneto89/desarolla-aplicaciones-web/-/blob/main/parcial1/parcial1/PracticaWebDatos/registrarDatos.html

-Practica #6 - 23/09/2022 - Practica Web con bases de datos - Mostrar registros en consulta de datos
consultarDatos.php     -     COMMIT:198a00e79ed85246242a3f25895150ce8bb6a217
conexion.php           -     COMMIT:0133f7467d0f8576dc30e20d356df79c35f2cd28
